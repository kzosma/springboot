package com.springbootlearning.learningspringboot3;

import java.util.Properties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Learningspringboot3Application {
	
	private static final String SERVER_PORT = "server.port";
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(Learningspringboot3Application.class);
		// Add Properties
		Properties properties = new Properties();
		properties.put(SERVER_PORT, 8082);
		application.setDefaultProperties(properties);
		application.run(args);
	}
}
