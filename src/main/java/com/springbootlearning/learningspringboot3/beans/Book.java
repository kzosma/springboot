package com.springbootlearning.learningspringboot3.beans;

import java.io.Serializable;
import lombok.Data;

@Data
public class Book implements Serializable {

	private static final long serialVersionUID = -5806400690661221651L;

	// Constructor with no args
	public Book() {super();}
	
	// Private fields
	private String name;
	private String author;
	private String editor;
}
