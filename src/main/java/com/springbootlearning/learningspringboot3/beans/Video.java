package com.springbootlearning.learningspringboot3.beans;

import java.io.Serializable;

import lombok.Data;

@Data
public class Video implements Serializable {

	private static final long serialVersionUID = -2689662359676885926L;
	public Video() {
		super();
	}
	private String name;
	private String location;
	
	

}
