package com.springbootlearning.learningspringboot3.conf;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.springbootlearning.learningspringboot3.beans.Book;
import com.springbootlearning.learningspringboot3.beans.Video;
import com.springbootlearning.learningspringboot3.services.YouTubeService;

@Configuration
public class BeansConfiguration {

    @Bean
    Book book() {
		return new Book();
	}
    @Bean
    Video video() {
		return new Video();
	}
    
    @Bean
    @ConditionalOnProperty(prefix = "app.properties", name = "youtube", havingValue = "true")
    YouTubeService youTubeService() {
    	return new YouTubeService();
    }

}
