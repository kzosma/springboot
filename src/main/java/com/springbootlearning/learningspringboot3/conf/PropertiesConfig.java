package com.springbootlearning.learningspringboot3.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:properties/conf.properties")
public class PropertiesConfig {
	
	@Value(value="${app.name}")
	private String app_name;
	public String getApp_name() {
		return app_name;
	}
}
