package com.springbootlearning.learningspringboot3.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import com.springbootlearning.learningspringboot3.conf.PropertiesConfig;
import com.springbootlearning.learningspringboot3.properties.AppProperties;

@Component
public class DataInitializer implements ApplicationRunner {
	
	@Autowired
	ApplicationContext applicationContext;
	
	@Autowired
	AppProperties properties;
	
	@Autowired
	PropertiesConfig propertiesConfig;
	
	@Value("${server.port}")
	private String server_port;
	
	private static final Logger log = LoggerFactory.getLogger(DataInitializer.class);
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("DataInitializer Runner");
		String[] beansName = applicationContext.getBeanDefinitionNames();
		if(beansName != null && beansName.length > 0) {
			for(String bean: beansName) {
				log.info("Bean : " + bean + " is registered into application context");
			}
		}
		log.info("App properties header : " +properties.getHeader());
		log.info("App properties footer : " +properties.getFooter());
		log.info("App Name : " +propertiesConfig.getApp_name());
		log.info("App Server Port : " +server_port);
		
	}
}
