package com.springbootlearning.learningspringboot3;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class Learningspringboot3ApplicationTests {

	@Test
	void contextLoads() {
	}

}
